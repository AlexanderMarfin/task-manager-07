package ru.tsc.marfin.tm;

import ru.tsc.marfin.tm.constant.ArgumentConst;
import ru.tsc.marfin.tm.constant.TerminalConst;
import ru.tsc.marfin.tm.model.Command;
import ru.tsc.marfin.tm.util.NumberUtil;

import java.util.Scanner;

public final class Application {

    public static void main(final String[] args) {
        if (processArgument(args)) System.exit(0);
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.INFO:
                showSystemInfo();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp(); 
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                showErrorCommand(command);
                break;
        }
    }

    public static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showErrorArgument(arg);
                break;
        }
    }

    public static boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = isMemoryLimit ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        final long userMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(userMemory);
        System.out.println("User memory in JVM: " + usedMemoryFormat);
    }

    public static void showWelcome() {
        System.out.println("** Welcome to Task-Manager **");
    }

    public static void showErrorCommand(String arg) {
        System.err.printf("Error! This argument `%s` is not supported \n", arg);
    }

    public static void showErrorArgument(String arg) {
        System.err.printf("Error! This argument `%s` is not supported \n", arg);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexander Marfin");
        System.out.println("E-mail: amarfin@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("1.2.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.ABOUT);
        System.out.println(Command.VERSION);
        System.out.println(Command.HELP);
        System.out.println(Command.EXIT);
    }

    public static void close() {
        System.exit(0);
    }

}
